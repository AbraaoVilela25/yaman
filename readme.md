#Documenta��o Ambiente QA

##Indice

- Entendendo o ambiente;
- Ferramentas utilizadas;
- Java JDK (JAVA DEVELOPMENT KIT) V.15 (Vers�o est�vel at� o momento);
- Como instalar Eclipse IDE para desenvolvedores Java. (vers�o est�vel at� o momento);
- Iniciando um novo projeto Maven;
- Como instalar Selenium WebDriver;
- Como encontrar e instalar reposit�rios desejados;
- Iniciando um novo projeto;


## Entendendo o Ambiente.

<p>Define-se como um conjunto de atividades para garantir a qualidade nos processos de desenvolvimento - afim de garantir a maior seguran�a no seu funcionamento.<p>
<p>Automa��o de teste � o uso de software para controlar a execu��o do teste de software, a compara��o dos resultados esperados com os resultados reais, a configura��o das pr�-condi��es de teste e outras fun��es de controle e relat�rio de teste.<p>

### Ferramentas utilizadas.

- Eclipse IDE para desenvolvedores Java.

Descri��o do Pacote.

As ferramentas essenciais para qualquer desenvolvedor Java, incluindo um IDE Java, um cliente Git, Editor XML, Mylyn, Maven e integra��o Gradle

   Este pacote inclui:  
     
   Integra��o Git para Eclipse.  
   Eclipse Java Development Tools.  
   Integra��o Maven para Eclipse
   Lista de Tarefas Mylyn.  
   Ferramentas de recomenda��es de c�digo para desenvolvedores Java.  
   Editores e ferramentas do Eclipse XML.  
   
- ChromeDriver

<p> WebDriver � uma ferramenta de c�digo aberto para teste automatizado de aplicativos da web em v�rios navegadores. Ele fornece recursos para navegar para p�ginas da web, entrada do usu�rio, execu��o de JavaScript e muito mais. ChromeDriver � um servidor aut�nomo que implementa  o padr�o W3C WebDriver . ChromeDriver est� dispon�vel para Chrome em Android e Chrome em Desktop (Mac, Linux, Windows e ChromeOS).  
   
- Java JDK (JAVA DEVELOPMENT KIT) V.15 (Vers�o est�vel at� o momento)
	
<p>Java Development Kit significa Kit de Desenvolvimento Java, e � um conjunto de utilit�rios que permitem criar sistemas de software para a plataforma Java. � composto por compilador e bibliotecas.<p>

- Integra��o Maven.

<p>Maven � uma ferramenta de automa��o de constru��o usada principalmente para projetos Java. O Maven tamb�m pode ser usado para construir e gerenciar projetos escritos em C #, Ruby, Scala e outras linguagens. ... Maven � constru�do usando uma arquitetura baseada em plug-in que permite fazer uso de qualquer aplicativo control�vel por meio de entrada padr�o.<p>

- Selenium + WebDriver.

<p>Selenium � um conjunto de ferramentas de c�digo aberto multiplataforma, usado para testar aplica��es web pelo browser de forma automatizada. ... O Selenium suporta diversas linguagens de programa��o, como por exemplo C#, Java e Python, e v�rios navegadores web como o Chrome e o Firefox.<p>

<p>O WebDriver conduz um navegador nativamente, como um usu�rio faria, localmente ou em uma m�quina remota usando o servidor Selenium, marca um salto em termos de automa��o do navegador.  
Selenium WebDriver se refere �s liga��es de linguagem e �s implementa��es do c�digo de controle do navegador individual. Isso � comumente referido como apenas WebDriver.<p>
<p>Selenium WebDriver � um Recomenda��o W3C.<p>    
<p>O WebDriver foi projetado como uma interface de programa��o simples e mais concisa.<p>  
<p>WebDriver � uma API compacta orientada a objetos.<p>
<p>Ele conduz o navegador de forma eficaz.<p>


####Eclipse IDE para desenvolvedores Java. (vers�o est�vel at� o momento);

<p>- Visite em seu browser para download da IDE : https://www.eclipse.org/downloads/packages/release/oxygen/3a/eclipse-ide-java-developers.<p> 
<p> - Instalando IDE Eclipse.<p>   
<p> - (1) Clique no box (install) - no seu diret�rio Downloads, execute arquivo da IDE Eclipse - escolha op��o (Eclipse IDE Java Developers) - Install.<p>   
<p> - (1.1) Conclu�do instala��o no seu Desktop aparecer� um �cone da IDE Eclipse - se a instala��o for realizada corretamente n�o aprensentar� nenhum erro, podendo seguir com o projeto - Double click para inicia-lo.<p>  
<p> - (1.2) Escolha o melhor diret�rio em sua m�quina para alocar seu WorkSpace (onde todos seus projetos ficar�o salvos) eu utilizo o diret�rio e recomendo que utilizem o mesmo : - c:\Projetos_Automacao.<p> 
<p> - (1.3) A tela principal do eclipse ser� apresentada, n�o entrarei em detalhes sobre o �Eclipse�, pois o foco desse tutorial se baseia na automa��o com o selenium Webdriver, mas algumas informa��es/dicas est�o no pacote.<p>  

#####Como configurar o ambiente de testes. 

- Primeiro 

<p> (1.a) No eclipse no canto esquerdo superior clique em File > New > Project.<p>
<p> (1.b) No wizard project selecione o campo Maven Project > Next <p>
<p> (1.c) No wizard New Maven Project no campo Group Id preencher com o dominio desejado ou qual esteja automatizando - ex: .br.com.qualquerdominio.webdriverJava - Artifact Id preencher o nome do projeto desejado - ex: webdriverJava<p>
<p> (1.d) Clicar no bot�o finish <p>
<p> (2.a) No projeto abrir pasta "SRC" achar POM.XML<p>
<p> (2.b) No navegador digite "mvnrepository.com" - digite Selenium WebDriver - pesquisar - selecione a vers�o mais estavel no momento � 3.141.59 - copie o repositorio<p>
<p> (2.c) No eclipse v� at� a pasta SRC - POM.XML - pressione enter antes de </project> - crie <dependencies><dependencies> - de um enter entre as dependencies e cole o repositorio do selenium webdriver que adquirimos no mvnrepository<p>
<p> (3.a) Agora podemos come�ar o nosso teste automatizado com Selenium WebDriver + Java<p>
<p> (3.b) Na pasta do seu projeto "src/main/java" - com bot�o direito new > package > crie um package chamado como preferir, sugiro que crie um padr�o por exemplo runner - dentro do package runner crie uma classe > bot�o direito > new > Class > Main<p>
<p> (3.c) Na class Main - crie um metodo public static void main(String[] args){ } - na pasta Src-main-java - crie um package chamado commons - dentro do package commoms um parametro chamado StartParameters - atribua - WebDriver driver; public WebDriver getDriver() { retun driver; } public void setDriver(WebDriver driver) { this.driver = driver; }<p>
<p> (3.d) Dessa forma deixamos o WedDriver estanciado dentro de um parametro. 
<p> (3.e) Dentro do metodo main puxe o parametro StartParameters - StartParameters sttp = new StartParameters(); - toda vez que formos chamar o metodo driver � s� chamar sttp.getDriver(); 

<p> (3.f) Crie um package - navigation - dentro do package crie uma Class - AcessaPagina - dentro do Acessa Pagina importe o Parametro StartParameters <p>
<p> (3.g) public static void (StartParameters sttp);fa�a a chamada do sttp.setDriver(new ChromeDriver());sttp.setDriver(new ChromeDriver());<p>  <p> (3.h) String url = "https://www.shoestock.com.br/"; - sttp.getDriver().get(url); - String titulo = sttp.getDriver().getTitle(); <p> 
<p> (3.i) System.out.println("Esse � o titulo da pagina : " + titulo); - salve e run aplication - Chrome devera dar um start e entrar na url desejada.<p>
<p> (3.g) Dentro do metodo main inclua o metodo AcessaPagina - AcessaPagina.acessaPagina(sttp);<p>
<p> (4.a) Dentro do package navigation - crie a Class IncluirProduto<p>
<p> (4.b) Crie o metodo public static void incluirProduto(StartParameters sttp) throws InterruptedException { }<p>
<p> (4.c) Dentro das chaves do metodo - sttp.getDriver().findElement(By.id("search-input")).sendKeys("T�nis");<p>
<p> (4.d) sttp.getDriver().findElement(By.xpath("/html/body/div[2]/header/div/div/section[2]/section/form/div/button")).click();<p>
<p> (4.e) sttp.getDriver().findElement(By.linkText("T�nis Shoestock Comfy Tricot Feminino")).click(); 
<p> (4.f) sttp.getDriver().close();
<p) (5.a) Atribua o metodo IncluirProduto dentro do metodo Acessapagina - IncluirProduto.incluirProduto(sttp);

	
	

	

 

 
