package navigation;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import commons.StartParameters;

public class IncluirProduto {
	
	public static void incluirProduto(StartParameters sttp) throws InterruptedException {
		
		sttp.getDriver().findElement(By.id("search-input")).sendKeys("T�nis");
		
		sttp.getDriver().findElement(By.xpath("/html/body/div[2]/header/div/div/section[2]/section/form/div/button")).click();
		
		sttp.getDriver().findElement(By.linkText("T�nis Shoestock Comfy Tricot Feminino")).click();
		
		String vendidoCapturado = "T�nis Shoestock Comfy Tricot Feminino - Preto ";
		String validacaoNomeProduto = sttp.getDriver().findElement(By.xpath("//*[@id=\"content\"]/div[2]/section/section[1]/h1")).getText();
				if (vendidoCapturado.equals(validacaoNomeProduto)) {
						System.out.println("Valida��o Envio Capturado Com Sucesso");
				}
				
		String textoValidacao = sttp.getDriver().findElement(By.xpath("//*[@id=\"buy-box\"]/div[2]")).getText();
			if(textoValidacao.contains("Vendido e Enviado por Shoestock")) {
					System.out.println("valida��o Vendido Capturada com Sucesso");
			}
			if(textoValidacao.contains("at� 4x de R$ 57,48")) {
				    System.out.println("Valida��o Parcelamento Capturado com Sucesso");
			}
			if(textoValidacao.contains("R$ 229,90")) {
				    System.out.println("Valida��o Pre�o Capturado Com Sucesso");
		}
	}
}
