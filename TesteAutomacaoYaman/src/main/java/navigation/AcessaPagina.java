package navigation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import commons.StartParameters;

public class AcessaPagina {

	public static void acessaPagina(StartParameters sttp) throws InterruptedException {
		
		sttp.setDriver(new ChromeDriver());
		sttp.getDriver().manage().window().maximize();
		
		sttp.getDriver().manage().timeouts().implicitlyWait(5,	TimeUnit.SECONDS);
		String url = "https://www.shoestock.com.br/";

		sttp.getDriver().get(url);

		String titulo = sttp.getDriver().getTitle();

		System.out.println("Esse � o titulo da pagina : " + titulo);
		
		sttp.getDriver().manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);

		IncluirProduto.incluirProduto(sttp);
	}
}
