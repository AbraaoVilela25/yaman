package br.com.testeyaman.api;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import junit.framework.Assert;

public class Trello extends BaseTest {
	
	@Test
	
	public void test() {
		
		test = extent.createTest("Teste Trello");
		this.resp = RestAssured.get("https://api.trello.com/1/actions/592f11060f95a3d3d46a987a");
		
		code = resp.getStatusCode();
		corpo = resp.asString();
		
		Assert.assertEquals(code, 200);
		
		test.info("corpo da resposta: \n" + corpo );
		if(corpo.contains("Professional")) {
			System.out.println("Validação Campo Name Capturado com Sucesso");
		}
				
		
	}

}
